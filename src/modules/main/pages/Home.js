export default function Home() {
    return <div className={'text-center'}>
        <h1 className={'display-4'}>Добро пожаловать на сайт</h1>
        <div className={'display-4'}>Вас ожидает самый лучший контент в интернете</div>
    </div>
}