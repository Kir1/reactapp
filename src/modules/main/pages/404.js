export default function Page404() {
    return (
        <div className={'text-center'}>
            <h1>404</h1>
            <b className={'display-4'}>Страница не найдена</b>
        </div>
    )
}