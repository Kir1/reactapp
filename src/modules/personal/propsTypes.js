import PropTypes from 'prop-types';
import {PROFESSIONS as list} from "./constants";
export const PROP_TYPE_ONE_OF_PROFESSION = PropTypes.oneOf(list);

export const PROP_TYPE_WORKER = PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    birthday: PropTypes.string.isRequired,
    role: PROP_TYPE_ONE_OF_PROFESSION.isRequired,
    isArchive: PropTypes.bool.isRequired
}).isRequired;