import {DROPDOWN_VARIANT_NOT_CHOSEN} from "../../../ui/constants";
import {PROFESSIONS_NAMES_WITH_VARIANT_NOT_CHOSEN} from "../constants";

export const employees = state => {
    let returnArray = [...state.personal.employees];

    if (state.personal.role !== DROPDOWN_VARIANT_NOT_CHOSEN) {
        returnArray = returnArray.filter(
            (worker) => worker.role === state.personal.role
        );
    }

    //показываем работников из архива, либо всех
    returnArray = returnArray.filter(
        (worker) => state.personal.isArchive === false || (state.personal.isArchive === true && worker.isArchive === state.personal.isArchive)
    );

    return returnArray;
}

export const getWorker = (id) => state => {
    return state.personal.employees.find(worker => worker.id === Number(id));
}

export const getIsArchive = state => {
    return state.personal.isArchive;
}

export const getRole = state => {
    return state.personal.role;
}

export const getRoleName = state => {
    return PROFESSIONS_NAMES_WITH_VARIANT_NOT_CHOSEN[state.personal.role];
}