import {createSlice} from '@reduxjs/toolkit';
import {employees as employeesList} from '../employees';
import {sortObjectsArrayByProperty} from '../../../helpers/array';
import moment from 'moment';
import {DROPDOWN_VARIANT_NOT_CHOSEN} from "../../../ui/constants";
import {updateObjectInStateById, removeObjectInStateById} from "../../../helpers/redux";

export const counterSlice = createSlice({
    name: 'personal',
    initialState: {
        idCounter: employeesList.length,
        employees: employeesList,
        role: DROPDOWN_VARIANT_NOT_CHOSEN,
        isArchive: false
    },
    reducers: {
        sortByName: state => {
            state.employees = sortObjectsArrayByProperty(state.employees, 'name');
        },
        sortByBirthday: state => {
            state.employees = sortObjectsArrayByProperty(state.employees, 'birthday', false,
                (date) => moment(date, 'DD.MM.YYYY').unix()
            );
        },
        filterByProfession: (state, action) => {
            state.role = action.payload;
        },
        filterByIsArchive: (state, action) => {
            state.isArchive = action.payload;
        },
        addWorker: (state, action) => {
            state.idCounter++;
            state.employees = [
                {
                    ...action.payload,
                    id: state.idCounter
                },
                ...state.employees
            ]
        },
        updateWorker: (state, action) => {
            state.employees = updateObjectInStateById(state.employees, action.payload);
        },
        removeWorker: (state, action) => {
            state.employees = removeObjectInStateById(state.employees, action.payload);
        },
    },
});

export const {sortByName, sortByBirthday, filterByProfession, filterByIsArchive, addWorker, updateWorker, removeWorker} = counterSlice.actions;

export default counterSlice.reducer;
