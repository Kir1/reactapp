import PropTypes from 'prop-types';
import React from 'react';
import {Table, DropdownButton, Dropdown} from 'react-bootstrap';
import Worker from './Worker';
import {Button, Form} from 'react-bootstrap';
import {PROP_TYPE_WORKER} from "../propsTypes";
import {useDispatch} from 'react-redux';
import {
    sortByName,
    sortByBirthday,
    filterByProfession,
    filterByIsArchive
} from '../redux/slice';
import {
    PROFESSIONS_NAMES_WITH_VARIANT_NOT_CHOSEN,
    PROFESSIONS_WITH_VARIANT_NOT_CHOSEN
} from "../constants";
import {useSelector} from "react-redux";
import {getRoleName, getIsArchive} from "../redux/selectors";

function List({workers}) {
    const dispatch = useDispatch();

    let isArchive = useSelector(getIsArchive);
    let professionChosenName = useSelector(getRoleName);

    const isArchiveToggle = () => {
        dispatch(filterByIsArchive(!isArchive));
    };

    const setProfession = (professionValue) => {
        dispatch(filterByProfession(professionValue));
    };
    const professionListItem = PROFESSIONS_WITH_VARIANT_NOT_CHOSEN.map((professionValue) =>
        <Dropdown.Item
            key={professionValue}
            onClick={() => setProfession(professionValue)}
        >
            {PROFESSIONS_NAMES_WITH_VARIANT_NOT_CHOSEN[professionValue]}
        </Dropdown.Item>);

    const workersList = workers.map((worker) => <Worker key={worker.id} worker={worker}/>);

    return (
        <React.Fragment>
            <div className={'mb-2 font-weight-bold'}>Сортировка:</div>
            <Button className={'mr-2'} variant="primary" onClick={() => dispatch(sortByName())}>По
                имени</Button>
            <Button variant="primary" onClick={() => dispatch(sortByBirthday())}>По дате рождения</Button>

            <div className={'my-3'}>
                <b>Фильтр по професии:</b>
                <DropdownButton className={'mt-2'} title={professionChosenName}>
                    {professionListItem}
                </DropdownButton>
            </div>

            <b>Фильтр по архиву:</b>
            <Form.Group controlId="archiveCheckbox">
                <Form.Check type="checkbox" label="Работники в архиве" checked={isArchive}
                            onChange={() => isArchiveToggle()}/>
            </Form.Group>

            <div className={'mt-3'}>
                {workers.length > 0 ?
                    <Table responsive className="workers">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Телефон</th>
                            <th>Дата рождения</th>
                            <th>Профессия</th>
                            <th>В архиве</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {workersList}
                        </tbody>
                    </Table>
                    :
                    <div className={'display-4'}>Работники не найдены, либо отсутсвуют</div>
                }
            </div>
        </React.Fragment>
    );
}

List.propTypes = {
    workers: PropTypes.arrayOf(PROP_TYPE_WORKER).isRequired,
}

export default List;