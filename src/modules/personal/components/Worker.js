import {PROP_TYPE_WORKER} from "../propsTypes.js";
import {PROFESSIONS_NAMES} from "../constants";
import {Nav} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import {Button} from 'react-bootstrap';
import {removeWorker} from '../redux/slice';
import {useDispatch} from 'react-redux';

function Worker({worker}) {
    const dispatch = useDispatch();
    let classNames = require('classnames');
    let trClasses = classNames('worker', {'worker_in-archive': worker.isArchive});
    return (
        <tr key={worker.id.toString()} className={trClasses}>
            <td className={"worker__name"}>
                <Nav.Link as={NavLink} to={{pathname: `/worker/${worker.id}`}}>{worker.name}</Nav.Link>
            </td>
            <td className={"worker__phone"}>{worker.phone}</td>
            <td className={"worker__birthday"}>{worker.birthday}</td>
            <td className={"worker__role"}>{PROFESSIONS_NAMES[worker.role]}</td>
            <td className={"worker__is-archive"}>{worker.isArchive ? "В архиве" : "Не в архиве"}</td>
            <td className={"worker__delete"}>
                <Button variant="danger" onClick={() => dispatch(removeWorker(worker.id))}>Удалить</Button>
            </td>
        </tr>
    )
}

Worker.propTypes = {
    worker: PROP_TYPE_WORKER
}

export default Worker;