import {Form, Col, Button, Alert} from 'react-bootstrap';
import InputMask from 'react-input-mask';
import React, {useState} from 'react';
import {PROP_TYPE_WORKER} from "../propsTypes";
import {REG_EXPRS_DATE, REG_EXPRS_PHONE} from "../../../helpers/regularExrpessions";
import {PROFESSIONS, PROFESSIONS_NAMES} from "../constants";
import PropTypes from 'prop-types';
import {useDispatch} from "react-redux";
import {possibleConversionToBoolean} from "../../../helpers/dataTypes";

function FactoryForm({worker, btnName, actionFunction, successText}) {
    const dispatch = useDispatch();

    const [actionSuccess, setActionSuccess] = useState(false);
    const [form, changeForm] = useState(worker);

    const handleSubmit = (event) => {
        event.preventDefault();
        if (form.name && REG_EXPRS_PHONE.test(form.phone) && REG_EXPRS_DATE.test(form.birthday)) {
            setActionSuccess(true);
            dispatch(actionFunction(form));
        }
    };

    function changeField(property, value) {
        let formClone = {...form};
        formClone[property] = value;
        changeForm(formClone);
    }

    function onChange(property, value) {
        changeField(property, value);
    }

    function onChangeIsArchive() {
        changeField('isArchive', !possibleConversionToBoolean(form.isArchive));
    }

    const professionListItem = PROFESSIONS.map((professionValue) =>
        <option key={professionValue} value={professionValue}>
            {PROFESSIONS_NAMES[professionValue]}
        </option>);

    return <React.Fragment>
        <Form noValidate onSubmit={handleSubmit}>
            <Form.Row>
                <Form.Group as={Col} md="4">
                    <Form.Label>Имя</Form.Label>
                    <Form.Control
                        required
                        type="text"
                        placeholder="Имя"
                        value={form.name}
                        isInvalid={!form.name}
                        isValid={form.name}
                        onChange={(ev) => onChange('name', ev.target.value)}
                    />
                    <Form.Control.Feedback>Имя введено правильно</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                        Введите имя
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} md="4">
                    <Form.Label>Телефон</Form.Label>
                    <InputMask mask="+7 (999) 999-9999" value={form.phone}
                               onChange={(ev) => onChange('phone', ev.target.value)}
                               inputRef=""
                    >
                        {(inputProps) => <Form.Control
                            {...inputProps}
                            required
                            isInvalid={!REG_EXPRS_PHONE.test(form.phone)}
                            isValid={REG_EXPRS_PHONE.test(form.phone)}
                            type="text"
                            placeholder="Номер телефона"
                        />}
                    </InputMask>
                    <Form.Control.Feedback>Номер корректный</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                        Введите корректный номер телефона.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} md="4">
                    <Form.Label>Дата рождения</Form.Label>
                    <InputMask mask="99.99.9999" value={form.birthday}
                               onChange={(ev) => onChange('birthday', ev.target.value)}>
                        {(inputProps) => <Form.Control
                            {...inputProps}
                            required
                            isInvalid={!REG_EXPRS_DATE.test(form.birthday)}
                            isValid={REG_EXPRS_DATE.test(form.birthday)}
                            type="text"
                            placeholder="Дата рождения"
                        />}
                    </InputMask>
                    <Form.Control.Feedback>Дата рождения введена</Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                        Введите дату рождения.
                    </Form.Control.Feedback>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} md="3">
                    <Form.Label>Профессия</Form.Label>
                    <Form.Control as="select" value={form.role} onChange={(ev) => onChange('role', ev.target.value)}>
                        {professionListItem}
                    </Form.Control>
                </Form.Group>
            </Form.Row>
            <Form.Group controlId="archiveCheckbox">
                <Form.Check label="В архиве" checked={form.isArchive} onChange={() => onChangeIsArchive()}/>
            </Form.Group>
            <Button type="submit">{btnName}</Button>
        </Form>

        {actionSuccess &&
        <Alert variant="success" onClose={() => setActionSuccess(false)} dismissible className={'mt-5'}>
            <Alert.Heading>Успех!</Alert.Heading>
            <p>{successText}</p>
        </Alert>
        }
    </React.Fragment>
}

FactoryForm.propTypes = {
    worker: PROP_TYPE_WORKER,
    btnName: PropTypes.string.isRequired,
    actionFunction: PropTypes.func.isRequired,
    successText: PropTypes.string.isRequired,
}

export default FactoryForm;