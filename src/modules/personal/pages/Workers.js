import {useSelector} from "react-redux";
import {employees} from "../redux/selectors";
import List from '../components/List';
import '../styles/Workers.scss';

export default function Workers() {
    return <List workers={useSelector(employees)}/>;
}