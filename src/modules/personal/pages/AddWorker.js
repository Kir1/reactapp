import Form from '../forms/FactoryForm';
import {PROFESSION_DRIVER} from "../constants";
import {addWorker} from '../redux/slice';

export default function AddWorker() {
    let worker = {
        "id": 0,
        "name": "",
        "isArchive": false,
        "role": PROFESSION_DRIVER,
        "phone": "",
        "birthday": ""
    }
    return (
        <div>
            <h1>Создание нового работника</h1>
            <Form worker={worker} btnName={'Создать работника'} actionFunction={(worker)=> addWorker(worker)} successText={'Работник добавлен в систему'}/>
        </div>
    );
}