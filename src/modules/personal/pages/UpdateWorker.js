import Form from '../forms/FactoryForm';
import {updateWorker} from '../redux/slice';
import {getWorker} from '../redux/selectors';
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";

export default function UpdateWorker() {
    let {id} = useParams();
    let worker = useSelector(getWorker(id));
    return (
        <div>
            {
                worker ?
                    <div>
                        <h1>Редактирование работника "{worker.name}"</h1>
                        <Form worker={worker} btnName={'Сохранить'} actionFunction={(worker) => updateWorker(worker)}
                              successText={'Сохранение завершено.'}/>
                    </div>
                    :
                    <h3>Пользователя не существует</h3>
            }
        </div>
    );
}