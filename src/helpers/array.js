export function sortObjectsArrayByProperty(array, property, ascOrDesc = true, propertyGetter = (value) => value) {
    return [...array].sort(
        (a, b) => {
            a = propertyGetter(a[property]);
            b = propertyGetter(b[property]);
            if (ascOrDesc) return (a > b) ? 1 : ((b > a) ? -1 : 0)
            return (a < b) ? 1 : ((b < a) ? -1 : 0)
        }
    )
}