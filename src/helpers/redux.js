export function updateObjectInState(stateArray, newObject, checkObjectFunction) {
    const newArray = [...stateArray];
    const index = newArray.findIndex(obj => checkObjectFunction(obj));
    if (index >= 0) newArray[index] = newObject;
    return newArray;
}

export function updateObjectInStateById(stateArray, newObject) {
    return updateObjectInState(stateArray, newObject, obj => obj.id === newObject.id);
}

export function removeObjectInStateById(stateArray, id) {
    const newArray = [...stateArray];
    const index = newArray.findIndex(obj => obj.id === id);
    if (index >= 0) newArray.splice(index, 1);
    return newArray;
}