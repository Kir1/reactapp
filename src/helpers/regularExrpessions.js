export const REG_EXPRS_DATE = /^\d{2}\.\d{2}\.\d{4}$/;
export const REG_EXPRS_PHONE = /^\+7 \(\d{3}\) \d{3}-\d{4}$/;