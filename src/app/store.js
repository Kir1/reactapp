import {configureStore} from '@reduxjs/toolkit';
import personalSlice from '../modules/personal/redux/slice';
import logger from 'redux-logger';

export default configureStore({
    reducer: {
        personal: personalSlice,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});