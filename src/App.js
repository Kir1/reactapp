import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
} from "react-router-dom";
import {Container, Nav, Col, Row, Navbar} from 'react-bootstrap';
import HomePage from './modules/main/pages/Home';
import Page404 from './modules/main/pages/404';
import WorkersPage from './modules/personal/pages/Workers';
import AddWorkerPage from './modules/personal/pages/AddWorker';
import UpdateWorkerPage from './modules/personal/pages/UpdateWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import './modules/main/styles/main.scss';

function App() {
    return (
        <Router>
            <Container id="application">
                <Row className="justify-content-md-center">
                    <Col>
                        <Navbar bg="light" expand="lg" className={'mb-3 mb-sm-5'}>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">
                                    <Nav.Link as={NavLink} to="/" exact>Главная</Nav.Link>
                                    <Nav.Link as={NavLink} to="/workers">Работники</Nav.Link>
                                    <Nav.Link as={NavLink} to="/add-worker">Добавить работника</Nav.Link>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>

                        <Switch>
                            <Route exact path="/">
                                <HomePage/>
                            </Route>
                            <Route path="/workers">
                                <WorkersPage/>
                            </Route>
                            <Route path="/add-worker">
                                <AddWorkerPage/>
                            </Route>
                            <Route path="/worker/:id">
                                <UpdateWorkerPage/>
                            </Route>
                            <Route path="*">
                                <Page404/>
                            </Route>
                        </Switch>
                    </Col>
                </Row>
            </Container>
        </Router>
    );
}

export default App;